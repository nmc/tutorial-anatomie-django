#! /usr/bin/env node
/*jshint esversion: 6 */
/* eslint comma-dangle: ["error", {"functions": "never"}] */
/* eslint no-console: ["error", { allow: ["log"] }] */
/* eslint func-names: ["error", "never"] */


/*
* Add the steps to take to create deployable css files. I.e. compile sass,
* minify, uglify, union, copy, ...
*/

const sh = require('shelljs');
const sass = require('node-sass');
const fs = require('fs');
const getDirName = require('path').dirname;

const pathStatic = 'project/static/';

/*
  Render tutorial anatomie css
*/
sass.render(
  {
    file: 'project/app/src/scss/tutorial-anatomie_main.scss',
    includePaths: ['node_modules/bootstrap/scss'],
    outFile: 'project/static/css/tutorial-anatomie.css',
    outputStyle: 'compressed'
  },
  function (error, result) { // node-style callback from v3.0.0 onwards
    console.log(`=> render css to: ${this.options.outFile}`);
    if (error) {
      console.log(error.status); // used to be 'code' in v2x and below
      console.log(error.column);
      console.log(error.message);
      console.log(error.line);
    } else {
      if (!sh.test('-e', getDirName(this.options.outFile))) {
        sh.mkdir('-p', getDirName(this.options.outFile));
      }

      fs.writeFile(this.options.outFile, result.css, (err) => {
        if (err) throw err;
        console.log('SUCCESS');
      });
    }
  }
);

// Copy font files to static file folder

if (!sh.test('-e', `${pathStatic}font`)) {
  sh.mkdir('-p', `${pathStatic}font`);
}

// sh.cp('project/anatomie_bewegungsapparat/src/font/*.*', `${pathStatic}font/`);
