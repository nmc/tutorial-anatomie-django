#!/bin/bash

export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python
source /usr/local/bin/virtualenvwrapper.sh
source $WORKON_HOME/tutorial-anatomie-django/bin/activate

# Exit code of this script, set to 1 if any of the checks fails
EXIT_CODE=0
set_exit_code ()
{
	if [ $1 -ne 0 ];
	then
		EXIT_CODE=1;
	else
		printf " all fine.\n"
	fi;
}

printf "\nLINT: run pylint-django\n"
pylint -E -d C,R --load-plugins pylint_django --disable=no-member project/config project/app --exclude=migrations,node_modules project/
set_exit_code "$?"

printf "\nLINT: run flake8\n"
flake8 --exclude=migrations,node_modules project/
set_exit_code "$?"

printf "\nLINT: run sass-lint\n"
# config file: ../.sass-lint.yml
./node_modules/sass-lint/bin/sass-lint.js -v
set_exit_code "$?"

printf "\nLINT: check js\n"
./node_modules/.bin/eslint .
set_exit_code "$?"

exit $EXIT_CODE
