FROM library/archlinux:base
EXPOSE 8000

RUN echo 'Server = http://pkg.adfinis-sygroup.ch/archlinux/$repo/os/$arch' > /etc/pacman.d/mirrorlist
RUN pacman -Syyu --noconfirm
RUN pacman -S --noconfirm iputils postgresql python python-pip gcc linux-headers openjpeg2 tar

# prepare log folder
# Create source code environment
RUN mkdir -p /opt/src
WORKDIR /opt/src

RUN mkdir -p /opt/media
RUN chmod -R a+w /opt/media

ADD . ./
WORKDIR /opt/src/project


# install django app
RUN sh -c "pip install -r ../requirements/production.txt"

USER 1001

CMD bash -c "python manage.py runserver 0.0.0.0:8000 --settings=config.settings.production"
