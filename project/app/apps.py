from django.contrib.admin.apps import AdminConfig

class TuanaAdminConfig(AdminConfig):
    default_site = 'app.admin.TuanaAdminSite'
