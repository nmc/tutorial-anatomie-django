import random
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response

import app.serializers as serialize

from app.models import Answer, Element, ElementMedia, Media, Question, Structure
import logging

# Get an instance of a logger
logger = logging.getLogger('django')

class ListAnswers(APIView):
    def get(self, request, topic, format=None):
        rawList = Answer.objects.raw(
            """
            SELECT *, app_question_answers.answer_text, app_question_answers.question AS question_id
            FROM app_question
            INNER JOIN element_{0}
            ON app_question.element_id = element_{0}.id
            INNER JOIN app_question_answers
            ON app_question.id = app_question_answers.question
            ;
            """.format(topic)
        )
        ansList = [serialize.answer(item) for item in set(rawList)]
        return Response({
            'count': len(ansList),
            'results': ansList
        })

class ListMedia(APIView):
    def get(self, request, topic, format=None):
        rawList = Media.objects.raw(
               """
               SELECT *, app_mediatyp.name AS typ_name
               FROM app_media
               INNER JOIN app_elementmedia
               ON app_media.id = app_elementmedia.media_id
               INNER JOIN element_{0}
               ON app_elementmedia.element_id = element_{0}.id
               INNER JOIN app_mediatyp
               ON app_media.typ_id = app_mediatyp.id;
               """.format(topic)
               )
        mediaList = [serialize.media(m) for m in set(rawList)]
        return Response({
            'count': len(mediaList),
            'results': mediaList
        })

class ListElementMedia(APIView):
    def get(self, request, topic, format=None):
        rawList = ElementMedia.objects.raw(
            """
            SELECT *, app_media.name AS media_name, element_{0}.name_med AS element_name
            FROM app_elementmedia
            INNER JOIN element_{0}
            ON app_elementmedia.element_id = element_{0}.id
            INNER JOIN app_media
            ON app_elementmedia.media_id = app_media.id;
            """.format(topic)
        )
        elemMedList = [serialize.elementMedia(item) for item in set(rawList)]
        return Response({
            'count': len(elemMedList),
            'results': elemMedList
        })


class ListQuestions(APIView):
    def get(self, request, topic, format=None):
        rawList = Question.objects.raw(
            """
            SELECT *
            FROM app_question
            INNER JOIN element_{0}
            ON app_question.element_id = element_{0}.id;
            """.format(elementView)
        )
        elemList = [serialize.question(item) for item in set(rawList)]
        return Response({
            'count': len(elemList),
            'results': elemList
        })

class TestSet(APIView):
    def get(self, request, topic, format=None):
        # We want to delete 'learn' modus question -> modus=0
        # Unfortuately all 'exam' modus questions are secondary
        # So I have keep requesting learn question for the first question
        # Let's find a solution with Oksana very, very soon...
        # fstQuestionList = Question.objects.raw(
        #     """
        #     SELECT *, app_question.id AS question_id, {0}_withmedia.name_med AS element_name
        #     FROM app_question
        #     INNER JOIN {0}_withmedia
        #     ON app_question.element_id = {0}_withmedia.id
        #     WHERE app_question.published AND app_question.is_primary AND app_question.modus = 0 AND app_question.is_mc = 'f'
        #     ORDER BY random() LIMIT 20
        #     """.format(elementView)
        # )
        # sndQuestionList = Question.objects.raw(
        #     """
        #     SELECT *, app_question.id AS question_id, {0}_withmedia.name_med AS element_name
        #     FROM app_question
        #     INNER JOIN {0}_withmedia
        #     ON app_question.element_id = {0}_withmedia.id
        #     WHERE app_question.published AND NOT app_question.is_primary AND app_question.modus = 1
        #     ORDER BY random() LIMIT 20
        #     """.format(elementView)
        # )

        # we have to make sure that we
        # 1) only use elements that have an image assigned
        #    -> we use the view element_with_media defined in ./urls.py
        # 2) only use (primary) questions that have a secondary question
        #    -> we use the view prim_sec_q defined in ./urls.py
        qSqlList = Question.objects.raw(
            """
            WITH element_q AS (
              SELECT *,
                     prim_sec_q.id AS question_id,
                     element_{0}_withmedia.name_med AS element_name,
                     ROW_NUMBER () OVER (PARTITION BY prim_sec_q.element_id) AS row
              FROM prim_sec_q
              INNER JOIN element_{0}_withmedia
              ON prim_sec_q.element_id = element_{0}_withmedia.id AND prim_sec_q.topic_id = (SELECT app_topic.id FROM app_topic WHERE app_topic.nick = '{0}')
            )
            SELECT * FROM element_q WHERE element_q.row = 1
            ORDER BY random() LIMIT 20
            """.format(topic)
        )
        qList = [serialize.testQuestion(item) for item in qSqlList]
        return Response(qList)

class ListStructures(APIView):
    def get(self, request, topic, format=None):
        rawList = Structure.objects.raw(
            """
            SELECT *
            FROM app_structure
            INNER JOIN app_element_structures
            ON app_structure.id = app_element_structures.structure_id
            INNER JOIN element_{0}
            ON app_element_structures.element_id = element_{0}.id;
            ;
            """.format(topic)
        )
        structList = []
        for item in set(rawList):
            sitem = serialize.structure(item)
            if sitem not in structList:
                structList.append(sitem)

        return Response({
            'count': len(structList),
            'results': structList
        })

class ListElements(APIView):
    def get(self, request, topic, format=None):
        import time
        startTime = time.time()
        rawList = Element.objects.raw("SELECT * FROM element_{0}".format(topic))
        print("Time 1: " + str(time.time() - startTime))
        elemList = [serialize.element(item) for item in rawList]
        print("Time 2: " + str(time.time() - startTime))
        # import pdb; pdb.set_trace()
        return Response({
            'count': len(elemList),
            'results': elemList
        })
