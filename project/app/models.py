from django.db import models

class Topic(models.Model):
    name = models.CharField(max_length=63, unique=True)
    nick = models.CharField(max_length=15, unique=True)
    def __str__(self):
        return self.name

class Structure(models.Model):
    name = models.CharField(max_length=255, unique=True)
    def __str__(self):
        return self.name

class Media(models.Model):
    typ = models.ForeignKey(
        'MediaTyp',
        on_delete=models.CASCADE,
        related_name='media',
        )
    url = models.ImageField(max_length=255, unique=True)
    name = models.CharField(max_length=255)
    beschreibung = models.TextField(max_length=255, blank=True, null=True)
    def __str__(self):
        return self.name

class MediaTyp(models.Model): # so far: Foto, Radiologie, Schnitt
    name = models.CharField(max_length=63, unique=True)
    def __str__(self):
        return self.name

class ElementMedia(models.Model):
    element = models.ForeignKey('Element', on_delete=models.CASCADE)
    media = models.ForeignKey('Media',
                              on_delete=models.CASCADE,
                              verbose_name='media')
    position = models.CharField(max_length=255, default='0,0')
    position_text = models.CharField(max_length=255, default='0,0')
    no = models.CharField(max_length=45, blank=True, null=True, default=1)
    is_overview = models.BooleanField(default=False)

    class Meta:
        unique_together = ('position', 'is_overview', 'media', 'element')

    @property
    def position_x(self):
        try:
            posX = self.position.split(',', 1)[0]
        except:
            return "0"
        else:
            return posX
    @property
    def position_y(self):
        try:
            posY = self.position.split(',', 1)[1]
        except:
            return "0"
        else:
            return posY
    @property
    def position_text_x(self):
        try:
            posX = self.position_text.split(',', 1)[0]
        except:
            return "0"
        else:
            return posX
    @property
    def position_text_y(self):
        try:
            posY = self.position_text.split(',', 1)[1]
        except:
            return "0"
        else:
            return posY

class Element(models.Model):
    name_alt = models.CharField(max_length=255,
                                verbose_name='Alternative Schreibweise',
                                null=True,
                                blank=True
                                )
    name_med = models.CharField(max_length=255,
                                verbose_name="Name",
                                unique=True
                                )
    topics = models.ManyToManyField(Topic)
    structures = models.ManyToManyField(Structure)
    media = models.ManyToManyField(
        Media,
        through='ElementMedia',
        related_name='elements'
    )
    def __str__(self):
        return self.name_med

class Question(models.Model):
    text = models.TextField(max_length=255)
    element = models.ForeignKey(
        'Element',
        on_delete=models.CASCADE,
        related_name='questions',
        )
    topic = models.ForeignKey(
        'Topic',
        on_delete=models.CASCADE,
        related_name='questions',
    )
    # is it a multiple choice question (or text)
    is_mc = models.BooleanField(default=False)
    # is it a primary or a secondary question?
    is_primary = models.BooleanField(default=True)
    correct_answers_needed = models.IntegerField(default=1)
    modus = models.IntegerField(default=1)
    published = models.BooleanField(default=False)

    class Meta:
        unique_together = (
            'correct_answers_needed', 'element', 'is_mc', 'is_primary', 'text', 'topic',
        )

    def __str__(self):
        return self.text

class QuestionAnswer(models.Model):
    question = models.ForeignKey('Question', on_delete=models.CASCADE)
    answer = models.ForeignKey('Answer', on_delete=models.CASCADE)
    correct = models.BooleanField(default=False)

    class Meta:
        unique_together = ('question', 'answer')


class Answer(models.Model):
    questions = models.ManyToManyField(
        Question,
        through='QuestionAnswer',
        related_name='answers'
    )
    text = models.TextField(max_length=255, blank=True, null=True, unique=True)

    element = models.OneToOneField(
        'Element',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )

    def __str__(self):
        if (self.element == None):
            return self.text
        else:
            return str(self.element)
