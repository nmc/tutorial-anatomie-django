var ACTIVE_COLOR = '#d2da5e';		//Should be constant, doesn't work in IE
var INACTIVE_COLOR = '#79a9c9';		//Should be constant, doesn't work in IE

var ACTIVE_COLOR_OVERVIEW = '#d2da5f';		//Should be constant, doesn't work in IE
var INACTIVE_COLOR_OVERVIEW = '#79a9ca';		//Should be constant, doesn't work in IE

var r; //the raphaeljs container variable
var pins = []; //the pint container variable
/*
 * Draw a pin
 * r			Raphael Object
 * x1			Starting point of line
 * y1			Starting point of line
 * x2			Ending point of line
 * y2			Ending point of line
 * text			Text which is written to the bubble
 *
 * return		[x,y] endpoint of the line
 */
function drawPin(r, x1, y1, x2, y2, text, color, isOverview) {
  isOverview = isOverview || false;
	var lineWidth = 4;
	var lineColor = color;
	var fillColor = "#FFF";
	var circleRadius = 15;
	var fontSize = 14;
	var fontFamily = "Calibri, Arial";

	x1 *= r.width;
	x2 *= r.width;
	y1 *= r.height;
	y2 *= r.height;

	var xStart = x1;
	var yStart = y1;
	var dX = x1 - x2;
	var dY = y1 - y2;

	var c = lineLength(x1, y1, x2, y2); // length of hypotenuse

	// Calculate the angle. Phi lies between -pi and pi.
	var phi = (c > 0)? Math.acos(dX/c) : 0;

	if (dY<0)
		phi *= -1;

  // Now, round the angle to a multiple of pi/4
	// We resize phi to lie between -4 and 4 and then use the round function
	// then we resize it back to -pi .. pi
	phi = Math.round(phi/Math.PI*4)*Math.PI/4;

	var xEnd = xStart - c*Math.cos(phi);
	var yEnd = yStart - c*Math.sin(phi);

	// Draw Line
	var path = r.path("M" + xStart + " " + yStart + "L" + xEnd + " " + yEnd);
	path.attr({"stroke-width": lineWidth, "stroke": lineColor});

	// Draw Shape
	if(isOverview){
		// Draw plane
		circleRadius *= 0.85;
		marker = r.rect(xEnd-circleRadius, yEnd-circleRadius,circleRadius*2,circleRadius*2);
	} else {
		// Draw cricle
		marker = r.circle(xEnd,yEnd, circleRadius);
	}
	marker.attr({"stroke-width": lineWidth, "stroke": lineColor, "fill": fillColor});
	marker.data("text", text);
	marker.click(pinClick);

	//Draw Label
	var label = r.text(xEnd,yEnd, text);
	label.attr({"font-size": fontSize, "font-family": fontFamily});
	label.data("text", text);
	label.click(pinClick);

  django.jQuery('input[type=text]#id_no').val();
	xEnd /= r.width;
	yEnd /= r.height;

	return [xEnd,yEnd];
}

function lineLength(x, y, x0, y0){
	return Math.sqrt( ( x -= x0 ) * x + ( y -= y0 ) * y );
}

function repaint(){
	//@todo: there is a bug where this doesn't work. IPhone homescreen app....
	r.clear();
	django.jQuery.each(pins,function(index, pin){
		drawPin(r, pin.x, pin.y, pin.x2, pin.y2, pin.text, pin.color, pin.isOverview);
	});
}

/*
 * r					Raphael Object
 * canvas				Canvas div (jQuery object)
 * positionText			Textfield to display the position (jQuery object)
 * positionBubbleText	Textfield to display the position of the bubble (jQuery object)
 * number				Textfield which holds the number (jQuery object)
 */

function createPinMouseHandler(r, canvas, positionText, positionBubbleText, number){
	var ie = (document.all) ? true : false;
	var mouseIsDown = false;
	var numberValue = 0;
	var startX;
	var startY;

	canvas.mousedown(function(e) {
		mouseIsDown = true;
		var mouseX = (ie) ? e.clientX : e.pageX;
		var mouseY = (ie) ? e.clientY : e.pageY;

		startX = mouseX - canvas.offset().left;
		startY = mouseY - canvas.offset().top;
		startX /= r.width;
		startY /= r.height;
		numberValue =  django.jQuery('input[type=text]#id_no').val();
	});

	canvas.mouseup(function(e) {
		mouseIsDown = false;
		var mouseX = (ie) ? e.clientX : e.pageX;
		var mouseY = (ie) ? e.clientY : e.pageY;
		var endX = mouseX - canvas.offset().left;
		var endY = mouseY - canvas.offset().top;

		endX /= r.width;
		endY /= r.height;

		// draw line
		r.clear();
		bubblePos = drawPin(r, startX, startY, endX, endY, numberValue, ACTIVE_COLOR);

		positionText.val(String(startX) + ',' + String(startY));
		positionBubbleText.val(String(bubblePos[0]) + ',' + String(bubblePos[1]));
    django.jQuery('input[type=text]#id_position').val(String(startX + "," + startY));
    django.jQuery('input[type=text]#id_position_text').val(String(endX + "," + endY));
	});

	canvas.mousemove(function(e) {

		if(! mouseIsDown) return;

		var mouseX = (ie) ? e.clientX : e.pageX;
		var mouseY = (ie) ? e.clientY : e.pageY;
		var endX = mouseX - canvas.offset().left;
		var endY = mouseY - canvas.offset().top;

		endX /= r.width;
		endY /= r.height;

		// draw line
		r.clear();
		bubblePos = drawPin(r, startX, startY, endX, endY, numberValue, ACTIVE_COLOR);
	});
}

function getActivePinPosition(){
	var pinPosition = "";
	django.jQuery.each(pins,function(index, pin){
		if(pin.color == ACTIVE_COLOR){
			if(pin.x2>0.5){
				pinPosition = "right";
			} else {
				pinPosition = "left";
			}
		}
	});
	return pinPosition;
}

function pinClick(){
	var pinNo = this.data("text");
	if (pinNo)
		django.jQuery("div[elementNo="+pinNo+"] div:last").click();
}
