from rest_framework.views import APIView
from general.serializer import SendFeedbackSerializer
from django.http import HttpResponse
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)

class SendFeedbackView(APIView):
    # Anybody may send feedback
    permission_classes = ()
    def post(self, request):
        logger.error('sending feedback!')
        serializer = SendFeedbackSerializer(
                data=request.data,
                context = { 'request': request }
            )
        serializer.send_feedback()
        return HttpResponse('Thanks for your feedback!')
