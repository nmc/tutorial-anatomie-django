from rest_framework import serializers
from django.core import mail
from django.conf import settings
import logging
import json

# Get an instance of a logger
logger = logging.getLogger("django")

class SendFeedbackSerializer(serializers.Serializer):

    feedbackType = serializers.CharField(required=True)
    description = serializers.CharField(required=True)
    browserInfo = serializers.CharField(required=True)
    appState = serializers.CharField(required=True)
    sender = serializers.CharField(required=True)

    def send_feedback(self):
        if self.is_valid():
            feedbackType = self.validated_data['feedbackType']
            description = self.validated_data['description']
            sender = self.validated_data['sender']
            browserInfo = self.validated_data['browserInfo']
            appState = self.validated_data['appState']
            feedbackData = {
                'technical': {
                    'receiver': ['notifications-nmc@unibas.ch'],
                    'subject': 'Technisches Problem mit Tutorial Anatomie'
                },
                'content': {
                    'receiver': ['notifications-nmc@unibas.ch', 'oksana.raabe@unibas.ch'],
                    'subject': 'Feedback zum Tutorial Anatomie'
                },
            }

            try:
                mailData = feedbackData[feedbackType]
            except KeyError:
                subject = (
                    "%s hat einen nicht existierenden Feedback-Typ angegeben" %
                    sender
                )
                receiver = feedbackData.technical.receiver
            else:
                subject = mailData['subject']
                receiver = mailData['receiver']


            message = """
                Beschreibung des Fehlers von {}:\n{}\n\n
            """.format(sender, description)

            email = mail.EmailMessage(subject, message,
                    settings.DEFAULT_FROM_EMAIL, receiver)
            email.attach('appState.json',
                    json.dumps(json.loads(appState), indent=2, sort_keys=True),
                    'text/json; charset=utf-8')
            email.attach('browserInfo.json',
                    json.dumps(json.loads(browserInfo), indent=2, sort_keys=True),
                    'text/json; charset=utf-8')
            email.send()
            logger.info('Sent email to {}'.format(receiver))


        else:
            message = """
            Fehler beim Senden der Feedback Form.\n\n
            Errors:\n{}\n\n
            Error_messages:\n{}
            """.format(self.errors, self.error_messages)
            logger.error(message)
