#!/bin/sh
# waitForDb.sh

set -e

cmd="$@"

until psql -h tuana-db -U tuana-dbuser tuana-db -c "\d"; do
  >&2 echo "DB is unavailable - sleeping"
  sleep 1
done

>&2 echo "DB is up - executing command"

exec $cmd
