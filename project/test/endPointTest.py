import unittest
from handleEndPointPaths import create_list_of_routes_and_responses

import logging

# Get an instance of a logger
logger = logging.getLogger('django')

class TestEndpoints(unittest.TestCase):
    def setUp(self):
        routes = create_list_of_routes_and_responses()
        self.validRouteList = routes[0]
        self.invalidRouteList = routes[1]

    def test_find_invalid_route(self):
        if(len(self.invalidRouteList) > 0):
            logger.error('Error, following routes are invalid:')
            for (rou, res) in self.invalidRouteList:
                logger.error('{}\n{}\n\n'.format(rou, res.status_code))
        self.assertEqual(len(self.invalidRouteList), 0)


if __name__ == '__main__':
    unittest.main()
