from itertools import repeat, chain
import json, requests, os

topicTuple = ['bewegungsapparat', 'topographische', 'neuro']
modeTuple = ['anschauen']
strukturTuple = ['all', 'Brust', 'Beckengürtel', 'Kopf']
bildTypTuple = ['all', 'Schnitt', 'Foto', 'Radiologie']
routeTuple = [
    [topic, mode, struktur, bildTyp]
    for topic in topicTuple
    for mode in modeTuple
    for struktur in strukturTuple
    for bildTyp in bildTypTuple
]

routeTuple.append(['bewegungsapparat', 'test'])

def strip_all_at_the_end_of_list(l):
    """ Strip keyword 'all' at the end of a list with strings """
    if l[-1] == 'all':
        return strip_all_at_the_end_of_list(l[:-1])
    return l

def get_route_list():
    # aus ['bewegunsapparat', 'test', 'all', 'all'] wird ['bewegunsapparat', 'test']
    stripped = list(map(strip_all_at_the_end_of_list, routeTuple))

    # aus ['bewegungsapparat', 'test'] wird ['bewegungsapparat/test']
    return list(map(lambda x: '/'.join(x), stripped))

def requestSingleRoute(route):
    print('Getting {}..'.format(route))
    response = requests.get('{}/{}'.format(os.environ['BACKEND_ADDRESS'], route))
    print('...got response: {}'.format(response))
    return response

def create_list_of_routes_and_responses():

    print('Following routes will be tested:')
    for r in get_route_list():
        print(r)

    routeAndRespones = [(
        route,
        requestSingleRoute(route)
    ) for route in get_route_list()]

    validRoutesAndJson = [(route, response.json()) for (route, response) in
                          routeAndRespones if response.status_code == 200]

    invalidRoutesAndJson = [(route, response) for (route, response) in
                          routeAndRespones if response.status_code != 200]

    return [validRoutesAndJson, invalidRoutesAndJson]

def writeJsonToFile():
    answerList = create_list_of_routes_and_responses()
    with open('valid.json', 'w') as outfile:
        json.dump(answerList[0], outfile)
    with open('invalid.json', 'w') as outfile:
        json.dump(answerList[1], outfile)
